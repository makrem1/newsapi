//
//  Endpoints.swift
//  NewsAPI
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import Combine


public enum EndpointType {
    case topHeadlines(country: String, category: String, page: Int, pageSize: Int)
    case everything(query: String, page: Int, pageSize: Int)
    
    func urlRequest(baseURL: String, apiKey: String) -> URLRequest {
        var components = URLComponents(string: "\(baseURL)/\(path)")!
        components.queryItems = queryItems + [URLQueryItem(name: "apiKey", value: apiKey)]
        return URLRequest(url: components.url!)
    }
    
    private var path: String {
        switch self {
        case .topHeadlines:
            return "top-headlines"
        case .everything:
            return "everything"
        }
    }
    
    private var queryItems: [URLQueryItem] {
        switch self {
        case let .topHeadlines(country, category, page, pageSize):
            return [
                URLQueryItem(name: "country", value: country),
                URLQueryItem(name: "category", value: category),
                URLQueryItem(name: "page", value: String(page)),
                URLQueryItem(name: "pageSize", value: String(pageSize))
            ]
        case let .everything(query, page, pageSize):
            return [
                URLQueryItem(name: "language", value: "fr"),
                URLQueryItem(name: "q", value: query),
                URLQueryItem(name: "page", value: String(page)),
                URLQueryItem(name: "pageSize", value: String(pageSize))
            ]
        }
    }
}
