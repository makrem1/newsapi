//
//  Config.swift
//  NewsAPI
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation


class Config {
    
    static let shared = Config()
    
    private var apiKey: String?
    private var baseUrl: String?
    
    private init() {
        guard let path = Bundle.main.path(forResource: "Configuration", ofType: "plist"),
              let configDict = NSDictionary(contentsOfFile: path),
              let apiKey = configDict["apiKey"] as? String,
              let baseUrl = configDict["baseURL"] as? String  else {
            fatalError("Failed to read configuration.plist file")
        }
        self.apiKey = apiKey
        self.baseUrl = baseUrl
    }
    
    
    func getApiKey() -> String {
        guard let apiKey = apiKey else {
            fatalError("API key not found in configuration.plist file")
        }
        return apiKey
    }
    
    func getBaseUrl() -> String {
        guard let baseUrl = baseUrl else {
            fatalError("Base URL not found in configuration.plist file")
        }
        return baseUrl
    }
    
}


