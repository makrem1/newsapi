//
//  Models.swift
//  NewsAPI
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation


public struct Models {
    
    public struct Config: Codable {
        let apiKey: String
        let baseURL: String
    }
    
    public enum Response {
        
        public struct NewsAPIResponse: Codable {
            public var status: String
            public var totalResults: Int
            public var articles: [Article]
        }
        
        public struct Article: Codable {
            public let source: Source?
            public let author: String?
            public let title: String?
            public let description: String?
            public let url: URL?
            public let urlToImage: URL?
            //            let publishedAt: Date?
            public let content: String?
        }
        
        public struct Source: Codable {
            public let id: String?
            public let name: String?
        }
    }
    
    
}

extension Models.Response.Article: Hashable, Equatable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.title)
    }
    
    public static func ==(lhs: Models.Response.Article, rhs: Models.Response.Article) -> Bool {
        return lhs.title == rhs.title
    }
}
