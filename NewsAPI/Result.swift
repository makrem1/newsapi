//
//  Result.swift
//  NewsAPI
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import Combine


public class Manager {
    
    // MARK: - Properties
    
    public static let shared = Manager()
    
    public var cancellables = Set<AnyCancellable>()
    private let networkManager = NetworkManager()

    
    
    // MARK: - Public methods
    public func allNews(key: String = "Apple", page: Int = 1, pageSize: Int = 10) -> AnyPublisher<Result<Models.Response.NewsAPIResponse, NetworkManager.NetworkError>, Never> {
        if pageSize > 100 {
            return Just(Result.failure(.invalidPageSize)).eraseToAnyPublisher()
        }
        
        let endpoint = EndpointType.everything(query: key, page: page, pageSize: pageSize)
        return networkManager.fetchNews(endpoint: endpoint)
            .map { Result.success($0) }
            .catch { error -> AnyPublisher<Result<Models.Response.NewsAPIResponse, NetworkManager.NetworkError>, Never> in
                return Just(Result.failure(error))
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    public func topHeadLines(country: String = "us", category: String = "General", page:Int = 1, pageSize: Int = 10) -> AnyPublisher<Result<Models.Response.NewsAPIResponse, NetworkManager.NetworkError>, Never> {
        if pageSize > 100 {
            return Just(Result.failure(.invalidPageSize)).eraseToAnyPublisher()
        }
        let endpoint = EndpointType.topHeadlines(country: country, category: category, page: page, pageSize: pageSize)
        return networkManager.fetchNews(endpoint: endpoint)
            .map { Result.success($0) }
            .catch { error -> AnyPublisher<Result<Models.Response.NewsAPIResponse, NetworkManager.NetworkError>, Never> in
                return Just(Result.failure(error))
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
}
