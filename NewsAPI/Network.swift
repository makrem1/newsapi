//
//  Network.swift
//  NewsAPI
//
//  Created by Makrem Chambah on 15/4/2023.
//

import Foundation
import Combine 

public class NetworkManager {
    
    public enum NetworkError: Error {
        case invalidPageSize
        case invalidResponse
        case invalidStatusCode(Int)
        case requestFailed(Error)
        case decodingFailed(Error)
    }
    
    private let session: URLSession
    
    public init(session: URLSession = .shared) {
        self.session = session
    }
    
    func fetchNews(endpoint: EndpointType) -> AnyPublisher<Models.Response.NewsAPIResponse, NetworkError> {
        let request = endpoint.urlRequest(baseURL: Config.shared.getBaseUrl(), apiKey: Config.shared.getApiKey())
        
        return self.session.dataTaskPublisher(for: request)
            .tryMap { (data, response) -> Data in
                guard let httpResponse = response as? HTTPURLResponse,
                      200..<300 ~= httpResponse.statusCode else {
                    throw NetworkError.invalidResponse
                }
                return data
            }
            .decode(type: Models.Response.NewsAPIResponse.self, decoder: JSONDecoder())
            .map { $0 }
            .mapError { error -> NetworkError in
                switch error {
                case is URLError:
                    return NetworkError.requestFailed(error)
                case is DecodingError:
                    return NetworkError.decodingFailed(error)
                default:
                    return NetworkError.requestFailed(error)
                }
            }
            .eraseToAnyPublisher()
    }
}
