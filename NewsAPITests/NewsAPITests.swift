//
//  NewsAPITests.swift
//  NewsAPITests
//
//  Created by Makrem Chambah on 15/4/2023.
//

import XCTest
import Combine
import Foundation
import NewsAPI
@testable import NewsAPI

final class NewsAPITests: XCTestCase {
    
    var cancellables: Set<AnyCancellable>!
    var networkManager: NetworkManager!
    var api =  Manager.shared
    
    
    override func setUpWithError() throws {
        
        cancellables = Set<AnyCancellable>()
        networkManager = NetworkManager()
    }
    
    override func tearDownWithError() throws {
        
        cancellables = nil
        networkManager = nil
//        api = nil
        super.tearDown()
        
    }
    
    func testAllNews() {
        let key = "example"
        let expectedResponse = expectation(description: "all news fetched successfully")
        var receivedResult: Result<Models.Response.NewsAPIResponse, NetworkManager.NetworkError>?
        let page = 1 
        self.api.allNews(key: key, page: page)
            .sink(receiveValue: { result in
                receivedResult = result
                expectedResponse.fulfill()
            })
            .store(in: &cancellables)
        wait(for: [expectedResponse], timeout: 10.0)
        XCTAssertNotNil(receivedResult)
        if let receivedResult = receivedResult {
            switch receivedResult {
            case .success(let response):
                XCTAssertNotNil(response.articles)
            case .failure(let error):
                XCTFail("Expected result success, but got \(error.localizedDescription)")
            }
        }
    }
    
     
    
    func testTopHeadlines() {
        let country = "ww" // or any valid country code
        let category = "business" // or any valid category
        let expectedResponse = expectation(description: "top headlines fetched successfully")
        var receivedResult: Result<Models.Response.NewsAPIResponse, NetworkManager.NetworkError>?
        self.api.topHeadLines(country: country, category: category)
            .sink(receiveValue: { result in
                receivedResult = result
                expectedResponse.fulfill()
            })
            .store(in: &cancellables)
        wait(for: [expectedResponse], timeout: 10.0)
        XCTAssertNotNil(receivedResult)
        if let receivedResult = receivedResult {
            switch receivedResult {
            case .success(let response):
                XCTAssertNotNil(response.articles)
            case .failure(let error):
                XCTFail("Expected result success, but got \(error.localizedDescription)")
            }
        }
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
