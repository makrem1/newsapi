# NewsAPI Lib

Ce composant est une bibliothèque (library) développée en Swift appelée "NewsAPI". Cette bibliothèque fournit une abstraction pour consommer les API de NewsAPI.org. Elle implémente les endpoints "GET /everything" et "GET /top-headlines" en utilisant le protocole HTTP.



## Fonctionnalités

The library is divided into several files:

- Récupérer une liste d'articles correspondant à une requête spécifiée à l'aide de la méthode allNews.

- Récupérer une liste des articles les plus récents pour un pays et une catégorie donnés à l'aide de la méthode topHeadLines.

- Définition des différents endpoints qui peuvent être appelés à partir de NewsAPI, en utilisant le type énuméré EndpointType.

- Récupération des informations de configuration nécessaires à partir d'un fichier de configuration configuration.plist.

- Utilisation de URLSession pour effectuer des appels réseau.


## Contenu du composant


Le composant est divisé en plusieurs fichiers :

- Result.swift : Ce fichier définit une classe Manager qui expose des méthodes pour récupérer des articles en utilisant les endpoints "GET /everything" et "GET /top-headlines". La méthode allNews retourne une liste d'articles qui correspondent à une requête spécifiée. La méthode topHeadLines retourne une liste des articles les plus récents pour un pays et une catégorie donnés.

- Endpoints.swift : Ce fichier définit les différents endpoints qui peuvent être appelés à partir de NewsAPI, en utilisant le type énuméré EndpointType. Les endpoints sont topHeadlines et everything. Chaque endpoint retourne une instance de URLRequest qui peut être utilisée pour appeler l'API.

- Config.swift : Ce fichier définit une classe Config qui récupère les informations de configuration nécessaires à partir d'un fichier de configuration configuration.plist. Il s'agit de l'URL de base de l'API et de la clé API.

- Network.swift : Ce fichier définit une classe NetworkManager qui utilise URLSession pour effectuer des appels réseau. Cette classe implémente également une énumération NetworkError qui décrit les différentes erreurs qui peuvent se produire lors de l'appel de l'API. La méthode fetchNews est utilisée pour appeler l'API et retourne un Publisher qui émettra des données de réponse (les articles) ou une erreur en cas de problème.

## Test unitaires

Les tests unitaires pour les méthodes allNews et topHeadLines sont inclus dans le code et sont exécutés à l'aide de XCTest.

## Utilisation

1. Ajoutez la bibliothèque à votre projet :

   - Téléchargez le code source.
   - Faites glisser le dossier "NewsAPI" dans votre projet Xcode.
   - Cochez la case "Copy items if needed".
2. Importez la bibliothèque dans votre fichier :

```swift
import NewsAPI
```
3. Utilisez les méthodes de la classe Manager pour récupérer les articles :

```swift
let api = Manager.shared
api.allNews(query: "Apple") { articles in
    // Traitement :D
}
```
